<?php

namespace WebWeChat\Http;

class Request
{
    /**
     * @param $url
     * @param array $data
     * @return mixed
     */
    public function request($url, array $data = array())
    {
        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

        if (!empty($data)) {
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($data));
        }

        $response = curl_exec($ch);

        curl_close($ch);

        return $response;

    }

}