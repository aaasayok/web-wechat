<?php

include 'common.php';

$uuid = $webWeChat->getUUID();

if ($webWeChat->getLogin($uuid)) {
    //var_dump($info);
    exit(json_encode(array('code' => 1, 'msg' => 'success')));
}

exit(json_encode(array('code' => 0, 'msg' => 'fail')));