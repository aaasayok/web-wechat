<?php

require_once __DIR__ . '/../vendor/autoload.php';

header('Content-type:text/html; charset=utf-8');

$config = include __DIR__ . '/../config/config.php';

$request = new \WebWeChat\Http\Request();
$webWeChat = new WebWeChat\WeChat\WeChat($request);
